const stock = [
    {
        id: "1",
        name: "stylos", 
        position: "room1"
    },
    {
        id : "2",
        name: "carton",
        position: "room2"
    },
    {
        id: "3",
        name: "chat", 
        position: "room3"
    },
    {
        id : "4",
        name: "clavier",
        position: "room4"
    },
    {
        id: "5",
        name: "rallonge", 
        position: "room5"
    },
    {
        id : "6",
        name: "papier",
        position: "room6"
    },
    {
        id: "7",
        name: "ballon", 
        position: "room7"
    },
    {
        id : "8",
        name: "pomme",
        position: "room8"
    },
    {
        id: "9",
        name: "lapin", 
        position: "room9"
    },
];

export default stock;
