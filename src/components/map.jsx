import React, {useState, useRef, useEffect} from 'react'; 
import { deflateRaw } from 'zlib';
import { StyledMap } from '../styles/StyledMap';


const ClickableMap = () => {
    const [locations, setLocations] = useState(
        JSON.parse(localStorage.getItem('draw-map')) || []
    );
    const canvasRef = useRef(null); 

    useEffect(() => {
        const canvas = canvasRef.current;
        const ctx = canvas.getContext('2d');
        ctx.clearRect(0, 0, 100, 100); 
        locations.forEach(location => deflateRaw(ctx, location))
    }, []);

    function handleCanvasClick(e) {
        const newLocation = {x: e.clientX, y: e.clientY}
        setLocations([...locations, newLocation])
    }

    function handleClear() {
        setLocations([]);
    }

    function handleUndo() {
        setLocations(locations.slice(0, -1));
    }

    return (
        <>
          <button onClick={handleClear}>Clear</button>
        <button onClick={handleUndo}>Undo</button>
        <StyledMap>
        <canvas
            ref={canvasRef}
            onClick={handleCanvasClick}
         />
        </StyledMap>
        </>
    )

}

export default ClickableMap;