import React from 'react';
import styled from 'styled-components'; 
import SearchBar from './searchBar';

const StyledMain = styled.div `
    .title {
        color: #ed7a40;
        text-align: center;
    }

    .title h1 {
        font-size: 400;
        padding: 20;
    }
`


const MainPage = () => {

    return (
        <StyledMain>
         <div className='title'>
             <h1>Find Your Stock</h1>
         </div>
         <SearchBar />
        </StyledMain>
    );
}

export default MainPage;