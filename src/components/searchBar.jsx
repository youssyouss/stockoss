import React, {useState, useEffect} from 'react'; 
import FontAwesome from 'react-fontawesome';
import { StyledSearchBarContent, StyledSearchBar } from '../styles/StyledSearchBar';
import styled from 'styled-components';

import stock from '../data/stock';

const StyledFloor = styled.div `
    grid-template-columns: auto auto auto ;
    display: grid;
    padding: 30px;
    width: 500px;
    height: 400px;
    margin: 0 auto;

    .room {
    border: 1px solid rgba(0, 0, 0, 0.8);
    padding: 20px;
    font-size: 20px;
    text-align: center;
    }
   .result {
        background-color: #f0b796;
    }


`

const StyledText = styled.div`
    font-family: 'Abel', sans-serif;
    text-align: center;
    margin: 0 auto;
`



const SearchBar = () => {
    const [searchTerm, setSearchTerm] = useState('');
    const [searchResults, setSearchResults] = useState([]);
    const handleSearch = (e) => {
        setSearchTerm(e.target.value);
        let res;
        res = document.getElementById(e.target.value);

        let oldId = searchTerm;

        if (res != null) {
            res.classList.toggle('result');
        }
        let oldRes; 
        oldRes = document.getElementById(oldId);
        if (!res){
            oldRes.classList.remove('result');
        }
    }

    useEffect(() => {
        const results = stock.filter(item =>
          item.id.toLowerCase().includes(searchTerm)
        );
        setSearchResults(results);
      
    }, [searchTerm]);

    


    return (
        <>
        <StyledSearchBar>
            <StyledSearchBarContent>
                <FontAwesome className="fa-search" name="search" size="2x" />
                <input
                type="text"
                placeholder="search for stock"
                onChange={handleSearch}
                value={searchTerm}
                />
            </StyledSearchBarContent>
        </StyledSearchBar>
       <StyledText>
       <p> Hey mate, here are all the elements you can look for. 
            <br/> Type Id in search bar to find where each item is located
         </p>
       <ul>
     {searchResults.map(item => (
       
            <div>{item.name} -  id : {item.id}</div>
         
          
     ))}
  </ul>
       </StyledText>
        <StyledFloor>
        <div className="room" id="1">room1</div>
        <div className="room" id="2">room2</div>
        <div className="room" id="3">room3</div>
        <div className="room" id="4">room4</div>
        <div className="room" id="5">room5</div>
        <div className="room" id="6">room6</div>
        <div className="room" id="7">room7</div>
        <div className="room" id="8">room8</div>
        <div className="room" id="9">room9</div>
        </StyledFloor>
        </>
    )
}

export default SearchBar; 
