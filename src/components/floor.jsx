import React from 'react'; 
import styled from 'styled-components';


const StyledFloor = styled.div `
    grid-template-columns: auto auto auto ;
    display: grid;
    padding: 30px;
    width: 500px;
    height: 400px;
    margin: 0 auto;

    .room {
    border: 1px solid rgba(0, 0, 0, 0.8);
    padding: 20px;
    font-size: 20px;
    text-align: center;
    }
    .room:hover {
        background-color: #f0b796;
    }


`



const FloorMap = ({callback}) => {
    return(
        <StyledFloor>
        <div className="room" id="r1">room1</div>
        <div className="room" id="r2">room2</div>
        <div className="room" id="r3">room3</div>
        <div className="room" id="r1">room4</div>
        <div className="room" id="r2">room5</div>
        <div className="room" id="r3">room6</div>
        <div className="room" id="r1">room7</div>
        <div className="room" id="r2">room8</div>
        <div className="room" id="r3">room9</div>
        </StyledFloor>
    );
}

export default FloorMap