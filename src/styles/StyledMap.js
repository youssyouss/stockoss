import styled from 'styled-components';
import map from '../assets/Entrepot_exemple.jpg';


export const StyledMap = styled.canvas `
    border: 1px solid #000;
    background: url(${map});
    width: 100%;
`
